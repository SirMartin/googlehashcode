class CacheToUse(object):
    def __init__(self, cacheId, size):
        self.cacheId = cacheId
        self.size = size
        self.freeSpace = size
        self.videoIds = []

class VideoCache(object):
    def __init__(self, videoId, endpointId, cacheId, time, datacenterTime, size):
        self.videoId = videoId
        self.endpointId = endpointId
        self.cacheId = cacheId
        self.size = size
        # Cache dictionary
        self.time = time
        self.datacenterTime = datacenterTime
        # Gain between datacenter and cache
        self.theNiceOne = 0

class Video(object):
    def __init__(self, videoId, endpointId, size):
        self.videoId = videoId
        self.size = size
        self.endpointId = endpointId
        # Datacenter dictionary
        self.datacenter = 0

class OutputCache(object):
    def __init__(self, cacheServerId, videoIds):
        self.cacheServerId = cacheServerId
        # Its an array with all the video ids.
        self.videoIds = videoIds

    def __str__(self):
        return "Cache server id {0} and amount of videos {1}".format(self.cacheServerId, len(self.videoIds))

class VideoRequest(object):
    def __init__(self, videoId, endpointId, numberOfRequest):
        self.videoId = videoId
        self.endpointId = endpointId
        self.numberOfRequest = numberOfRequest

    def __str__(self):
        return ""

class EndpointCacheConnection:
    def __init__(self, cacheId, latencyFromCache):
        self.cacheId = cacheId
        self.latencyFromCache = latencyFromCache

    def __str__(self):
        return ""

class Endpoint(object):
    def __init__(self, latencyFromDataCenter, numberCacheServersConnected, endpointCacheConnections):
        self.latencyFromDataCenter = latencyFromDataCenter
        self.numberCacheServersConnected = numberCacheServersConnected
        self.endpointCacheConnections = endpointCacheConnections

    def __str__(self):
        return "latencyFromDataCenter {0} -- numberCacheServersConnected {1} -- Amount of endpointCacheConnections {2}".format(self.latencyFromDataCenter, self.numberCacheServersConnected, len(self.endpointCacheConnections))

class Problem(object):
    def __init__(self, numberOfVideos, numberOfEndpoints, requestDescriptions, cacheServers, mbPerCacheServer, videoSizes, endpoints, requestList):
        self.numberOfVideos = numberOfVideos
        self.numberOfEndpoints = numberOfEndpoints
        self.requestDescriptions = requestDescriptions
        self.cacheServers = cacheServers
        self.mbPerCacheServer = mbPerCacheServer
        self.videoSizes = videoSizes
        self.endpoints = endpoints
        self.requestList = requestList

    def __str__(self):
        return "Problem with {0} videos and {1} endpoints, with {2} requestDescriptions,  cacheServers {3} and mb per server {4}. Amount video sizes: {5}, Amount of Endpoints {6}, Amount of request list {7}".format(self.numberOfVideos, self.numberOfEndpoints, self.requestDescriptions, self.cacheServers, self.mbPerCacheServer, len(self.videoSizes), len(self.endpoints), len(self.requestList))

def readFile(filename):
    # Declare the problem.
    problem = None

    videos, numberOfEndpoints, requestDescriptions, cacheServers, mbPerCacheServer, videoSizes, endpoints, requestList = 0,0,0,0,0,[],[],[]

    # Read the problem settings.
    lineNumber = 0
    lineEndpointNumber = 0
    isEndpoint = False
    tempEndpoint = None
    with open(filename, 'r') as fin:
        for line in fin:           
            if (lineNumber == 0):
                # Read the description.
                videos, numberOfEndpoints, requestDescriptions, cacheServers, mbPerCacheServer = [int(num) for num in line.split(' ')]
                lineNumber+=1
            elif(lineNumber == 1):
                # Read the sizes of videos.
                videoSizes = [int(num) for num in line.split(' ')]
                lineNumber+=1
            else:
                isEndpoint = len([int(num) for num in line.split(' ')]) == 2
                # Read the videos.
                if (isEndpoint):
                    if (tempEndpoint == None):
                        # Create the endpoint.
                        latencyFromDataCenter, numberCacheServersConnected = [int(num) for num in line.split(' ')]
                        tempEndpoint = Endpoint(latencyFromDataCenter, numberCacheServersConnected, [])

                        # Check if the endpoint has servers connected.
                        if (tempEndpoint.numberCacheServersConnected == 0):
                            endpoints.append(tempEndpoint)    
                            tempEndpoint = None;
                    else:
                        # Read cache servers
                        cacheId, latencyFromCache = [int(num) for num in line.split(' ')]
                        tempEndpoint.endpointCacheConnections.append(EndpointCacheConnection(cacheId, latencyFromCache))
                        lineEndpointNumber+=1
                        if (lineEndpointNumber == tempEndpoint.numberCacheServersConnected):
                            # Is the last one.
                            endpoints.append(tempEndpoint)                        
                            tempEndpoint = None
                            lineEndpointNumber = 0      
                else:
                    videoId, endpointId, numberOfRequest = [int(num) for num in line.split(' ')]
                    requestList.append(VideoRequest(videoId, endpointId, numberOfRequest))
    
    # Create the problem object
    problem = Problem(videos, numberOfEndpoints, requestDescriptions, cacheServers, mbPerCacheServer, videoSizes, endpoints, requestList)

    return problem 


def writeFile(filename, theoutput):
    with open(filename, 'w') as fout:
        # Write amount of cache servers.
        fout.write("{0}\n".format(len(theoutput)))
        
        # Write the output.
        for c in theoutput:
            fout.write("{0} {1}".format(c.cacheId, " ".join(str(item) for item in c.videoIds)) + "\n")

def findSolution(problem):
    print ("Find the very very best.")

    videoList = []

    cacheVideoEndpointList = []
   
    #!important
        #endpointDatacenterLatency = [x for x in problem.endpoints if x.latencyFromDataCenter == req.endpointId]

    weightConstant = 0.1

    # Read the request.
    for req in problem.requestList:
        # Check if the video exists in our list.
        existsVideo = [x for x in videoList if (x.endpointId == req.endpointId and x.videoId == req.videoId)]
        theEndpoint = problem.endpoints[req.endpointId]

        myVideo = None
        if (len(existsVideo) > 0):
            myVideo = existsVideo[0]
        else:
            myVideo = Video(req.videoId, req.endpointId, problem.videoSizes[req.videoId])
            for c in theEndpoint.endpointCacheConnections:
                cacheVideoEndpointList.append(VideoCache(req.videoId, req.endpointId, c.cacheId, 0, 0, myVideo.size))
        
        # Fill the data.
        endpointDatacenterLatency = theEndpoint.latencyFromDataCenter
        myVideo.datacenter += req.numberOfRequest * endpointDatacenterLatency

        # Caches
        for c in theEndpoint.endpointCacheConnections:
            time = req.numberOfRequest * c.latencyFromCache
            for ccc in cacheVideoEndpointList:
                if (ccc.videoId == req.videoId and ccc.endpointId == req.endpointId and ccc.cacheId == c.cacheId):
                    ccc.time += time
                    ccc.datacenterTime += myVideo.datacenter
                    ccc.theNiceOne = ((ccc.datacenterTime - ccc.time) * weightConstant) / myVideo.size

        # Add video request to the list.
        videoList.append(myVideo)

    # Try to find a solution.
    sorteredList = sorted(cacheVideoEndpointList, key=lambda x: x.theNiceOne, reverse=True)

    cachesToUse = []

    for vec in sorteredList:
        myCcc = [x for x in cachesToUse if (x.cacheId == vec.cacheId)]
        ctu = None

        if (len(cachesToUse) == 0):
            notExists = True
    
        for cache in cachesToUse:
            #if (vec.videoId == 16):
                #print("cacheId", cache.cacheId)
                #print("endpoint", vec.endpointId)
                #print(len(theEndpoint.endpointCacheConnections))
                #print("Res", len([x for x in theEndpoint.endpointCacheConnections if (x.cacheId == cache.cacheId)]) > 0)
                #print(cache.videoIds)
            existsCacheInTheEndpoint = len([x for x in theEndpoint.endpointCacheConnections if (x.cacheId == cache.cacheId)]) > 0
            if (existsCacheInTheEndpoint and len([x for x in cache.videoIds if (x == vec.videoId)]) > 0):
                notExists = False
            else:
                notExists = True
        
        if (len(myCcc) > 0):
            #Exists in the list
            ctu = myCcc[0]
            if (notExists and (ctu.freeSpace - vec.size >= 0) and len([x for x in ctu.videoIds if (x == vec.videoId)]) == 0):
                ctu.freeSpace -= vec.size
                ctu.videoIds.append(vec.videoId)
        else:
            #first time to use the cache
            ctu = CacheToUse(vec.cacheId, problem.mbPerCacheServer)
            if (notExists and (ctu.freeSpace - vec.size >= 0) and len([x for x in ctu.videoIds if (x == vec.videoId)]) == 0):
                ctu.freeSpace -= vec.size
                ctu.videoIds.append(vec.videoId)
                cachesToUse.append(ctu)

    return cachesToUse

def main():
    #file = "c:\Proyectos\googlehashcode\Chachitube\Docs\me_at_the_zoo.in"
    #file = "c:\Proyectos\googlehashcode\Chachitube\Docs\\videos_worth_spreading.in"
    file = "c:\Proyectos\googlehashcode\Chachitube\Docs\\trending_today.in"    
    #file = "c:\Proyectos\googlehashcode\Chachitube\Docs\kittens.in"

    outputfile = "c:\\Proyectos\\googlehashcode\\Chachitube\\output.in"

    print('Running on file: %s' % file)

    # Get the pizza information.
    problem = readFile(file)
    print (problem)
    
    # Check the reading integrity
    if (problem.numberOfVideos != len(problem.videoSizes)):
        print ("Problem reading videos")

    if (problem.numberOfEndpoints != len(problem.endpoints)):
        print ("Problem reading endpoints")
    
    if (problem.requestDescriptions != len(problem.requestList)):
        print ("Problem reading requests")
    
    problemIsNotCorrectlyReaded = problem.numberOfVideos != len(problem.videoSizes) or problem.numberOfEndpoints != len(problem.endpoints) or problem.requestDescriptions != len(problem.requestList)

    theoutput = []

    if not (problemIsNotCorrectlyReaded):
        try:
            theoutput = findSolution(problem)
        except KeyboardInterrupt:
            pass

        # Write the file.
        writeFile(outputfile, theoutput)
        print ("Output Done.")
    else:
        print ("ERROR")
    
# Start program
if __name__ == '__main__':
    main()