﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HashCodeConsoleApp
{
    #region Classes

    public class CacheToUse
    {
        public int CacheId { get; set; }
        public int Size { get; set; }
        public int FreeSpace { get; set; }
        public List<int> VideoIds { get; set; }
    }


    public class VideoCache
    {
        public int VideoId { get; set; }
        public int EndpointId { get; set; }
        public int CacheId { get; set; }
        public int Size { get; set; }
        // Cache dictionary
        public int Time { get; set; }
        public int DatacenterTime { get; set; }
        // Gain between datacenter and cache
        public double TheNiceOne { get; set; }
    }

    public class Video
    {
        public int VideoId { get; set; }
        public int Size { get; set; }
        public int EndpointId { get; set; }
        // Datacenter dictionary
        public int Datacenter { get; set; }
    }

    public class OutputCache
    {
        public int CacheServerId { get; set; }
        // Its an array with all the video ids.
        public List<int> VideoIds { get; set; }

        public override string ToString()
        {
            return string.Format("Cache server id {0} and amount of videos {1}", CacheServerId, VideoIds.Count);
        }
    }

    public class VideoRequest
    {
        public int VideoId { get; set; }
        public int EndpointId { get; set; }
        public int NumberOfRequest { get; set; }
    }

    public class EndpointCacheConnection
    {
        public int CacheId { get; set; }
        public int LatencyFromCache { get; set; }
    }

    public class Endpoint
    {
        public int LatencyFromDataCenter { get; set; }
        public int NumberCacheServersConnected { get; set; }
        public List<EndpointCacheConnection> EndpointCacheConnections { get; set; }

        public override string ToString()
        {
            return
                string.Format(
                    "latencyFromDataCenter {0} -- numberCacheServersConnected {1} -- Amount of endpointCacheConnections {2}",
                    LatencyFromDataCenter, NumberCacheServersConnected, EndpointCacheConnections.Count);
        }
    }

    public class Problem
    {
        public int NumberOfVideos { get; set; }
        public int NumberOfEndpoints { get; set; }
        public int RequestDescriptions { get; set; }
        public int CacheServers { get; set; }
        public int MbPerCacheServer { get; set; }
        public List<int> VideoSizes { get; set; }
        public List<Endpoint> Endpoints { get; set; }
        public List<VideoRequest> RequestList { get; set; }

        public override string ToString()
        {
            return
                string.Format(
                    "Problem with {0} videos and {1} endpoints, with {2} requestDescriptions,  cacheServers {3} and mb per server {4}. Amount video sizes: {5}, Amount of Endpoints {6}, Amount of request list {7}",
                    NumberOfVideos, NumberOfEndpoints, RequestDescriptions, CacheServers, MbPerCacheServer,
                    VideoSizes.Count, Endpoints.Count, RequestList.Count);
        }
    }

    #endregion

    class Program
    {
        const double WeightConstant = 1.5;

        static void Main(string[] args)
        {
            var startDate = DateTime.Now;
            Console.WriteLine("START: " + startDate.ToLongTimeString());

            //var file = @"..\..\..\..\Chachitube\Docs\me_at_the_zoo.in";
            //var file = @"..\..\..\..\Chachitube\Docs\videos_worth_spreading.in";
            var file = @"..\..\..\..\Chachitube\Docs\trending_today.in";
            //var file = @"..\..\..\..\Chachitube\Docs\kittens.in";

            var outputpath = @"..\..\..\..\HashCodeConsoleApp\HashCodeConsoleApp";

            Console.WriteLine($"Running on file: {file}");

            // Get the problem information.
            var problem = ReadFile(file);
            Console.WriteLine(problem.ToString());

            //var theoutput = FindSolution(problem);
            var cacheVideoEndpointList = FindSolutionWithDic(problem);

            var ts = DateTime.Now - startDate;
            Console.WriteLine("Elapsed Time: {0} Hours {1} Minutes and {2} Seconds", ts.Hours, ts.Minutes, ts.Seconds);

            var theoutput = GetCachesToUse(problem, cacheVideoEndpointList);

            // Write the file.
            var outputfile = Path.Combine(outputpath, Path.GetFileNameWithoutExtension(file) + "_output.in");
            WriteFile(outputfile, theoutput);
            Console.WriteLine("Output Done");
            ts = DateTime.Now - startDate;
            Console.WriteLine("Elapsed Time: {0} Hours {1} Minutes and {2} Seconds", ts.Hours, ts.Minutes, ts.Seconds);
            Console.ReadLine();
        }

        private static void WriteFile(string outputfile, List<CacheToUse> theoutput)
        {
            using (var sw = new StreamWriter(outputfile))
            {
                // Write amount of cache servers.
                sw.Write("{0}\n", theoutput.Count);

                // Write the output.
                foreach (var c in theoutput)
                {
                    sw.Write("{0} {1}", c.CacheId, string.Join(" ", c.VideoIds) + "\n");
                }
            }
        }

        private static string GetCacheVideoEndpointKey(int videoId, int endpointId, int cacheId)
        {
            return $"v{videoId}_e{endpointId}_c{cacheId}";
        }

        private static IEnumerable<VideoCache> FindSolutionWithDic(Problem problem)
        {
            var cacheVideoEndpointListDictionary = new Dictionary<string, VideoCache>();

            // Read the request.
            for (int i = 0; i < problem.RequestList.Count - 1; i++)
            {
                var req = problem.RequestList[i];
                // Check if the video exists in our list.
                var theEndpoint = problem.Endpoints[req.EndpointId];
                for (int j = 0; j < theEndpoint.EndpointCacheConnections.Count - 1; j++)
                {
                    VideoCache ccc = null;
                    var c = theEndpoint.EndpointCacheConnections[j];
                    var cacheVideoEndpointKey = GetCacheVideoEndpointKey(req.VideoId, req.EndpointId, c.CacheId);
                    if (!cacheVideoEndpointListDictionary.ContainsKey(cacheVideoEndpointKey))
                    {
                        ccc = new VideoCache
                        {
                            VideoId = req.VideoId,
                            EndpointId = req.EndpointId,
                            CacheId = c.CacheId,
                            Size = problem.VideoSizes[req.VideoId]
                        };

                        // Only if not exists.
                        cacheVideoEndpointListDictionary.Add(cacheVideoEndpointKey, ccc);
                    }

                    var time = req.NumberOfRequest * c.LatencyFromCache;
                    ccc = cacheVideoEndpointListDictionary[cacheVideoEndpointKey];
                    ccc.Time += time;
                    ccc.DatacenterTime += req.NumberOfRequest * theEndpoint.LatencyFromDataCenter;
                    //ccc.TheNiceOne = ((ccc.DatacenterTime - ccc.Time) * WeightConstant) / ccc.Size;
                    ccc.TheNiceOne = (ccc.DatacenterTime - ccc.Time);
                }
            }

            // Convert to list.
            var cacheVideoEndpointList = cacheVideoEndpointListDictionary.Select(x => x.Value);
            return cacheVideoEndpointList;
        }

        private static List<VideoCache> FindSolution(Problem problem)
        {
            var cacheVideoEndpointList = new List<VideoCache>();

            // Read the request.
            for (int i = 0; i < problem.RequestList.Count - 1; i++)
            {
                var req = problem.RequestList[i];
                // Check if the video exists in our list.
                var theEndpoint = problem.Endpoints[req.EndpointId];
                for (int j = 0; j < theEndpoint.EndpointCacheConnections.Count - 1; j++)
                {
                    var c = theEndpoint.EndpointCacheConnections[j];
                    var ccc = cacheVideoEndpointList.FirstOrDefault(x => x.VideoId == req.VideoId && x.EndpointId == req.EndpointId && x.CacheId == c.CacheId);
                    if (ccc == null)
                    {
                        // Only if not exists.
                        ccc = new VideoCache
                        {
                            VideoId = req.VideoId,
                            EndpointId = req.EndpointId,
                            CacheId = c.CacheId,
                            Size = problem.VideoSizes[req.VideoId]
                        };

                        cacheVideoEndpointList.Add(ccc);
                    }

                    var time = req.NumberOfRequest * c.LatencyFromCache;
                    ccc.Time += time;
                    ccc.DatacenterTime += req.NumberOfRequest * theEndpoint.LatencyFromDataCenter;
                    ccc.TheNiceOne = ((ccc.DatacenterTime - ccc.Time) * WeightConstant) / ccc.Size;
                }

                // Exit
                if (cacheVideoEndpointList.Count >= 50000) // 2 min 4 sec
                    break;
            }

            return cacheVideoEndpointList;
        }

        private static List<CacheToUse> GetCachesToUse(Problem problem, IEnumerable<VideoCache> cacheVideoEndpointList)
        {
            // Try to find a solution.
            var sorteredList = cacheVideoEndpointList.OrderByDescending(x => x.TheNiceOne).ToArray();

            var cachesToUse = new List<CacheToUse>();

            for (int i = 0; i < sorteredList.Length; i++)
            {
                var vec = sorteredList[i];

                // Check if some cache has space yet.
                if (cachesToUse.Any() && !cachesToUse.Any(x => x.FreeSpace - vec.Size >= 0) && cachesToUse.Count == problem.CacheServers)
                    continue;

                var ctu = cachesToUse.FirstOrDefault(x => x.CacheId == vec.CacheId);
                bool notExists = cachesToUse.Count == 0;

                foreach (var cache in cachesToUse)
                {
                    var existsCacheInTheEndpoint =
                        problem.Endpoints[vec.EndpointId].EndpointCacheConnections.Any(x => x.CacheId == cache.CacheId);
                    if (existsCacheInTheEndpoint && cache.VideoIds.Any(x => x == vec.VideoId))
                    {
                        notExists = false;
                    }
                    else
                    {
                        notExists = true;
                    }
                }

                if (ctu != null)
                {
                    // Exists in the list
                    if (notExists && (ctu.FreeSpace - vec.Size >= 0) && ctu.VideoIds.Count(x => x == vec.VideoId) == 0)
                    {
                        ctu.FreeSpace -= vec.Size;
                        ctu.VideoIds.Add(vec.VideoId);
                    }
                }
                else
                {
                    // First time to use the cache.
                    ctu = new CacheToUse
                    {
                        CacheId = vec.CacheId,
                        FreeSpace = problem.MbPerCacheServer,
                        Size = problem.MbPerCacheServer,
                        VideoIds = new List<int>()
                    };

                    if (notExists && (ctu.FreeSpace - vec.Size >= 0) && ctu.VideoIds.Count(x => x == vec.VideoId) == 0)
                    {
                        ctu.FreeSpace -= vec.Size;
                        ctu.VideoIds.Add(vec.VideoId);
                        cachesToUse.Add(ctu);
                    }
                }
            }
            return cachesToUse;
        }

        private static Problem ReadFile(string file)
        {
            var lines = System.IO.File.ReadAllLines(file);

            var problem = new Problem
            {
                Endpoints = new List<Endpoint>(),
                RequestList = new List<VideoRequest>()
            };

            var lineNumber = 0;
            var lineEndpointNumber = 0;
            Endpoint tempEndpoint = null;
            foreach (string line in lines)
            {
                if (lineNumber == 0)
                {
                    // Read the description.
                    var descriptionVariables = line.Split(' ');
                    problem.NumberOfVideos = int.Parse(descriptionVariables[0]);
                    problem.NumberOfEndpoints = int.Parse(descriptionVariables[1]);
                    problem.RequestDescriptions = int.Parse(descriptionVariables[2]);
                    problem.CacheServers = int.Parse(descriptionVariables[3]);
                    problem.MbPerCacheServer = int.Parse(descriptionVariables[4]);
                    lineNumber++;
                }
                else if (lineNumber == 1)
                {
                    // Read the sizes of videos.
                    problem.VideoSizes = line.Split(' ').Select(int.Parse).ToList();
                    lineNumber += 1;
                }
                else
                {
                    var lineSplitted = line.Split(' ');
                    var isEndpoint = lineSplitted.Length == 2;
                    // Read the videos.
                    if (isEndpoint)
                    {
                        if (tempEndpoint == null)
                        {
                            // Create the endpoint.
                            tempEndpoint = new Endpoint
                            {
                                LatencyFromDataCenter = int.Parse(lineSplitted[0]),
                                NumberCacheServersConnected = int.Parse(lineSplitted[1]),
                                EndpointCacheConnections = new List<EndpointCacheConnection>()
                            };

                            // Check if the endpoint has servers connected.
                            if (tempEndpoint.NumberCacheServersConnected == 0)
                            {
                                problem.Endpoints.Add(tempEndpoint);
                                tempEndpoint = null;
                            }
                        }
                        else
                        {
                            // Read cache servers
                            var ecc = new EndpointCacheConnection
                            {
                                CacheId = int.Parse(lineSplitted[0]),
                                LatencyFromCache = int.Parse(lineSplitted[1])
                            };
                            tempEndpoint.EndpointCacheConnections.Add(ecc);
                            lineEndpointNumber += 1;

                            if (lineEndpointNumber == tempEndpoint.NumberCacheServersConnected)
                            {
                                // Is the last one.
                                problem.Endpoints.Add(tempEndpoint);
                                tempEndpoint = null;
                                lineEndpointNumber = 0;
                            }
                        }
                    }
                    else
                    {
                        var videoRequest = new VideoRequest
                        {
                            VideoId = int.Parse(lineSplitted[0]),
                            EndpointId = int.Parse(lineSplitted[1]),
                            NumberOfRequest = int.Parse(lineSplitted[2])
                        };
                        problem.RequestList.Add(videoRequest);
                    }
                }
            }

            return problem;
        }
    }
}