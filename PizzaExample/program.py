import numpy

class PizzaSlice(object):
    def __init__(self, r1, c1, r2, c2):
        self.r1 = r1
        self.c1 = c1
        self.r2 = r2
        self.c2 = c2

class Pizza(object):
    def __init__(self, rows, columns, ingredientsPerSlice, maxCellsPerSlice, ingredientsMatrix):
        self.rows = rows
        self.columns = columns
        self.ingredientsPerSlice = ingredientsPerSlice
        self.maxCellsPerSlice = maxCellsPerSlice
        self.ingredientsMatrix = ingredientsMatrix

    def __str__(self):
        return "Pizza with {0} rows and {1} columns, with {2} minimum ingredients per slice and not more than {3} cells per slice.".format(self.rows, self.columns, self.ingredientsPerSlice, self.maxCellsPerSlice)

def readFile(filename):
    # Declare the pizza.
    pizza = None

    rows, columns, ingredientsPerSlice, maxCellsPerSlice, ingredientsMatrix = 0,0,0,0,None

    # Read the pizza settings.
    readedFirstLine = False
    with open(filename, 'r') as fin:
        line = fin.readline()

        if (readedFirstLine == False):
            # Read the pizza configuration.
            rows, columns, ingredientsPerSlice, maxCellsPerSlice = [int(num) for num in line.split(' ')]
        else:
            # Read the ingredients.
            for idx, ingredient in enumerate(list(line)):
                # The element with the same idx that the amount of columns is wrong, so I made an if there to fix the problem.abs
                if (idx < int(COLUMNS)):
                    print(idx, ingredient)
                    ingredientsMatrix[lineNumber][idx] = ingredient
    
    # Create the pizza object.abs
    pizza = Pizza(rows, columns, ingredientsPerSlice, maxCellsPerSlice, ingredientsMatrix)

    return pizza
    
def findSlices(pizza):
    print ("Find the best combination of slices.")

    # Create some slices as example.
    pizzaSlices = []

    pizzaSlices.append(PizzaSlice(0,0,1,2))
    pizzaSlices.append(PizzaSlice(0,3,2,4))
    #pizzaSlices.append(PizzaSlice(0,0,2,2))

    return pizzaSlices

def writeFile(filename, pizzaSlices):
    with open(filename, 'w') as fout:
        # Write amount of slices.
        fout.write("{0}\n".format(len(pizzaSlices)))
        
        # Write the slices.
        for ps in pizzaSlices:
            fout.write("{0} {1} {2} {3}".format(ps.r1, ps.c1, ps.r2, ps.c2) + "\n")

def main():
    file = "docs/example.in"
    outputfile = "docs/output.in"

    print('Running on file: %s' % file)

    # Get the pizza information.
    thePizza = readFile(file)

    # Find the best slices
    pizzaSlices = findSlices(thePizza)

    # Write the file.
    writeFile(outputfile, pizzaSlices)

    print ("Output Done.")
    
# Start program
if __name__ == '__main__':
    main()